
const init = () => {
  const menu = document.getElementById(`mobile`);
  const nav = document.getElementById(`navigation`);
  const close = document.getElementById(`close`);

  menu.addEventListener(`click`, function() {
    nav.style.display = `flex`;
    close.style.display = `flex`;
    menu.style.display = `none`;
  });

  close.addEventListener(`click`, function() {
    nav.style.display = `none`;
    close.style.display = `none`;
    menu.style.display = `flex`;
  });
};

init();
